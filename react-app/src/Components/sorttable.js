/**
 * Sorts table.
 * 
 * @param {HTMLTableElement} table
 * @param {number} column index of the column to sort
 * @param {boolean} asc determines sort ascending or descending
 */
function sortTable(table, column, asc = true) {
    const direction = asc ? 1 : -1;
    const body = table.tBodies[0];
    const rows = Array.from(body.querySelectorAll("tr"));

    //sort each row
    const sortedRows = rows.sort((a, b) => {
        const aCol = a.querySelector(`td:nth-child(${ column + 1 })`).textContent.trim();
        const bCol = b.querySelector(`td:nth-child(${ column + 1 })`).textContent.trim();
        return aCol > bCol ? (1 * direction) : (-1 * direction);
    });

    //Remove
    while (body.firstChild) {
        body.removeChild(body.firstChild);
    }

    //add
    body.append(...sortedRows);
    table.querySelectorAll("th").forEach(th => th.classList.remove("th-sort-asc", "th-sort-desc"));
    table.querySelector(`th:nth-child(${ column + 1})`).classList.toggle("th-sort-asc", asc);
    table.querySelector(`th:nth-child(${ column + 1})`).classList.toggle("th-sort-desc", !asc);
}

document.querySelectorAll(".table-sortable th").forEach(headerCell => {
    headerCell.addEventListener("click", () => {
        const tableElement = headerCell.parentElement.parentElement.parentElement;
        const headerIndex = Array.prototype.indexOf.call(headerCell.parentElement.children, headerCell);
        const currentDir = headerCell.classList.contains("th-sort-asc");
        sortTable(tableElement, headerIndex, !currentDir);
    });
});
