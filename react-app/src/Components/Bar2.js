import * as d3 from 'd3';
import React, { useRef, useEffect } from 'react';

function BarChart({ width, height, data }){
    const ref = useRef();

    useEffect(() => {
        const svg = d3.select(ref.current)
            .attr("width", width)
            .attr("height", height)
            .style("border", "1px solid black")
    }, []);

    useEffect(() => {
        draw();
    }, data);

    const draw = () => {
        const margin = ({top: 30, right: 0, bottom: 30, left: 40})


        var yScale = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.value)]).nice()
        .range([height - margin.bottom, margin.top]);

        var xScale = d3.scaleBand()
        .domain(d3.range(data.length))
        .range([margin.left, width - margin.right])
        .padding(0.01);

        var xAxis = (g) => g
        .attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(xScale).tickFormat(i => data[i].party).tickSizeOuter(0));

        var yAxis = (g) => g
        .attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(yScale).ticks())
        .call(g => g.select(".domain").remove())
        .call(g => g.append("text")
            .attr("x", -margin.left)
            .attr("y", 10)
            .attr("fill", "currentColor")
            .attr("text-anchor", "start")
            .text("Population (Millions)"));


        const svg = d3.select(ref.current);
      
        svg.append("g")
          .selectAll("rect")
          .data(data)
          .join("rect")
            .attr("x", (d, i) => xScale(i))
            .attr("y", d => yScale(d.value))
            .attr("height", d => yScale(0) - yScale(d.value))
            .attr("width", xScale.bandwidth())
            .attr("fill", d => d.color);
      
        svg.append("g")
            .call(xAxis);
      
        svg.append("g")
            .call(yAxis);
    }


    return (
        <div className="chart">
            <svg ref={ref}>
            </svg>
        </div>
        
    )

}

export default BarChart;