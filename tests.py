import unittest
import json, urllib.request, urllib.parse

"""
For information on the unit tests, please look in the Wiki

Make sure that the database configuration is set in dbArchitecture.py
"""

class TestModels(unittest.TestCase):
	def test_artist_1(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/artists?id=0").read())["artist_name"]
		self.assertEqual(response, "2Pac")
		print("7 ")

	def test_artist_2(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/artists?id=37").read())["artist_image_url"]
		self.assertEqual(response, "https://i.scdn.co/image/f8ace7e639f7342f6952496559535ec44b1e8a4d")
		print("8 ")

	def test_artist_3(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/artists?id=90").read())["artist_id"]
		self.assertEqual(response, 90)
		print("9 ")
#added
	def test_artist_4(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/artists?id=123").read())["artist_id"]
		self.assertEqual(response, 123)
		print("10 ")

	def test_album_1(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/albums?id=9").read())["album_name"]
		self.assertEqual(response, "Loyal To The Game")
		print("1 ")

	def test_album_2(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/albums?id=3487").read())["album_id"]
		self.assertEqual(response, 3487)
		print("2 ")

	def test_album_3(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/albums?id=3487").read())["album_numtracks"]
		self.assertEqual(response, 1)
		print("3 ")
#added
	def test_album_4(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/albums?id=1234").read())["album_numtracks"]
		self.assertEqual(response, 25)
		print("4 ")	

	def test_track_1(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/tracks?id=0").read())["track_duration"]
		self.assertEqual(response, 209)
		print("17 ")

	def test_track_2(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/tracks?id=100").read())["track_name"]
		self.assertEqual(response, "I Got A Woman (First Rehearsal)")
		print("18 ")

	def test_track_3(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/tracks?id=100").read())["track_album_id"]
		self.assertEqual(response, 457)
		print("19 ")
#added
	def test_track_4(self):
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/tracks?id=1234").read())["track_release_day"]
		self.assertEqual(response, "2018-03-16")
		print("20 ")

	def test_artist_album_relation(self):
		hasAlbumZero = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/artists?id=0").read())["artist_albums"]
		for i in response:
			if i["album_id"] == 0:
				hasAlbumZero = True
				break
		self.assertEqual(hasAlbumZero, True)
		print("11 ")

	def test_album_artist_relation(self):
		hasArtistZero = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/albums?id=0").read())["album_artists"]
		for i in response:
			if i["artist_id"] == 0:
				hasArtistZero = True
				break
		self.assertEqual(hasArtistZero, True)
		print("5 ")

	def test_artist_track_relation(self):
		hasTrack6287 = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/artists?id=0").read())["artist_tracks"]
		for i in response:
			if i["track_id"] == 6287:
				hasTrack6287 = True
				break
		self.assertEqual(hasTrack6287, True)
		print("12 ")

	def test_track_artist_relation(self):
		hasArtistZero = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/tracks?id=6287").read())["track_artists"]
		for i in response:
			if i["artist_id"] == 0:
				hasArtistZero = True
				break
		self.assertEqual(hasArtistZero, True)
		print("22 ")

	def test_album_track_relation(self):
		hasTrack6287 = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/albums?id=0").read())["album_tracks"]
		for i in response:
			if i["track_id"] == 6287:
				hasTrack6287 = True
				break
		self.assertEqual(hasTrack6287, True)
		print("6 ")

	def test_track_album_relation(self):
		inAlbumZero = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/tracks?id=6287").read())["track_album_id"] == 0
		self.assertEqual(inAlbumZero, True)
		print("21 ")

	def test_search_1(self):
		hasArtistZero = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/search?term=2pac").read())["artists"]
		for i in response:
			if i["artist_id"] == 0:
				hasArtistZero = True
				break
		self.assertEqual(hasArtistZero, True)
		print("13 ")

	def test_search_2(self):
		hasAlbum538 = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/search?term=music+to+be+murdered+by").read())["albums"]
		for i in response:
			if i["album_id"] == 538:
				hasAlbum538 = True
				break
		self.assertEqual(hasAlbum538, True)
		print("14 ")

	def test_search_3(self):
		hasTrack13308 = False
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/search?term=thriller").read())["tracks"]
		for i in response:
			if i["track_id"] == 13308:
				hasTrack13308 = True
				break
		self.assertEqual(hasTrack13308, True)
		print("15 ")
#added
	def test_search_4(self):
		hasTrack31208 = False
		url = urllib.parse.quote("no such thing")
		response = json.loads(urllib.request.urlopen("http://rockinwiththerona.me/api/search?term=" + url).read())["tracks"]
		for i in response:
			if i["track_id"] == 31208:
				hasTrack31208 = True
				break
		self.assertEqual(hasTrack31208, True)
		print("16 ")

if __name__ == '__main__': #pragma: no cover
	print("Tests passed (out of 22): ")
	unittest.main()