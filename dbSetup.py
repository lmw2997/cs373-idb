import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# Change these to your PostgreSQL Database configuration
pUsername = "postgres"
pPassword = "abc123"
pHost = "localhost"
pPort = "5432"
pDB = "rockinwiththerona"

"""
By running this Python file, all data needed for RockinTheRona will be 
gathered and stored into a PostgreSQL Database. Before running the file,
you will need a Spotify Developer Account and to enter these commands into
the terminal (Linux):

export SPOTIPY_CLIENT_ID="yourSpotifyClientID"
export SPOTIPY_CLIENT_SECRET="yourSpotifyClientSecret"
export SPOTIPY_REDIRECT_URI="yourAppRedirectURL"

Additionally, fill out the information about your PostgreSQL Database above.
"""

# Connect SQLAlchemy to PostgreSQL
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "postgres://"+pUsername+":"+pPassword+"@"+pHost+":"+pPort+"/"+pDB
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True # Disables warnings
db = SQLAlchemy(app)
print("Successfully connected SQLAlchemy to Database")

# Set up Many to Many relationship between artists and albums
artist_album = db.Table("artist_album",
    db.Column("artist_id", db.Integer, db.ForeignKey("artist.artist_id")),
    db.Column("album_id", db.Integer, db.ForeignKey("album.album_id"))
)

# Set up Many to Many relationship between artists and tracks
artist_track = db.Table("artist_track",
    db.Column("artist_id", db.Integer, db.ForeignKey("artist.artist_id")),
    db.Column("track_id", db.Integer, db.ForeignKey("track.track_id"))
)

# Create the classes for SQLAlchemy
class Artist(db.Model):
    """SQLAlchemy class for artists"""
    __tablename__ = "artist"
    artist_id = db.Column(db.Integer, primary_key=True)
    artist_name = db.Column(db.String)
    artist_albums = db.relationship("Album", secondary=artist_album, backref=db.backref("album_artists", lazy="dynamic"))
    artist_tracks = db.relationship("Track", secondary=artist_track, backref=db.backref("track_artists", lazy="dynamic"))
    artist_genres = db.Column(db.ARRAY(db.String))
    artist_image_url = db.Column(db.String)
    artist_spotify_url = db.Column(db.String)
    artist_spotify_id = db.Column(db.String)
    artist_hasinfo = db.Column(db.Boolean)

    def __init__(self, name, id, genres=[], image_url="", spotify_url="", spotify_id="", hasinfo=False):
        self.artist_id = id
        self.artist_name = name
        self.artist_genres = genres
        self.artist_image_url = image_url
        self.artist_spotify_url = spotify_url
        self.artist_spotify_id = spotify_id
        self.artist_hasinfo = hasinfo
    
    def __eq__(self, other):
        return self.artist_name == other.artist_name

class Album(db.Model):
    """SQLAlchemy class for albums"""
    __tablename__ = "album"
    album_id = db.Column(db.Integer, primary_key=True)
    album_name = db.Column(db.String)
    album_numtracks = db.Column(db.Integer)
    album_tracks = db.relationship("Track", backref="track_album")
    album_image_url = db.Column(db.String)
    album_release_day = db.Column(db.String)
    album_spotify_url = db.Column(db.String)
    album_spotify_id = db.Column(db.String)

    def __init__(self, name, id, numtracks=0, image_url="", release_day="", spotify_url="", spotify_id=""):
        self.album_id = id
        self.album_name = name
        self.album_numtracks = numtracks
        self.album_image_url = image_url
        self.album_release_day = release_day
        self.album_spotify_url = spotify_url
        self.album_spotify_id = spotify_id

class Track(db.Model):
    """SQLAlchemy class for tracks"""
    __tablename__ = "track"
    track_id = db.Column(db.Integer, primary_key=True)
    track_name = db.Column(db.String)
    track_duration = db.Column(db.Integer)
    track_album_id = db.Column(db.Integer, db.ForeignKey("album.album_id"))
    track_release_day = db.Column(db.String)
    track_image_url = db.Column(db.String)
    track_spotify_url = db.Column(db.String)
    track_preview_url = db.Column(db.String)
    track_spotify_id = db.Column(db.String)

# Commit to PostgreSQL Database
db.create_all()
db.session.commit()
print("Successfully set up relations between artists, albums, and tracks")

# Enter Spotify Developer credentials
spotify = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials())
print("Successfully connected to Spotify Database")

# Simpler classes for efficiency when finding duplicates in albums
class artist:
    """Simple class containing information about an artist"""
    def __init__(self, name, artist_id, albums=[], genres=[], image_url="", spotify_url="", spotify_id=""):
        self.name = name
        self.id = artist_id
        self.albums = albums
        self.genres = genres
        self.image_url = image_url,
        self.spotify_url = spotify_url
        self.spotify_id = spotify_id
    def __cmp__(self, other):
        if self.name > other.name:
            return 1
        elif self.name < other.name:
            return -1
        elif self.spotify_id > other.spotify_id:
            return 1
        elif self.spotify_id < other.spotify_id:
            return -1
        else:
            return 0
    def __eq__(self, other):
        return self.name == other.name and self.spotify_id == other.spotify_id
    def __repr__(self):
        return repr((self.name, self.id))

class album:
    """Simple class containing information about an album or a single"""
    def __init__(self, name, album_id, num_tracks, artists=[], tracks=[], image_url="", release_day="", spotify_url="", spotify_id=""):
        self.name = name
        self.id = album_id
        self.num_tracks = num_tracks
        self.artists = artists
        self.tracks = tracks
        self.image_url = image_url
        self.release_day = release_day
        self.spotify_url = spotify_url
        self.spotify_id = spotify_id
    def __cmp__(self, other):
        if self.name > other.name:
            return 1
        elif self.name < other.name:
            return -1
        elif self.spotify_id > other.spotify_id:
            return 1
        elif self.spotify_id < other.spotify_id:
            return -1
        else:
            return 0
    def __eq__(self, other):
        return self.name == other.name and sorted(self.artists) == sorted(other.artists)
    def __repr__(self):
        return repr((self.name, self.id))

artists = []
albums = []
Artists = []
Albums = []
Tracks = []
artist_id = 0
album_id = 0
track_id = 0
print("Gathering data")

# Add artists and their information
f = open("artistList.txt", "r")

for i in f:
    # Get the first artist returned when searching for an artist by their name
    result = spotify.search(i, type="artist")["artists"]["items"][0]
    if i == "Beck\n": # Beck is actually the second artist returned when searching for "Beck"
        result = spotify.search(i, type="artist")["artists"]["items"][1]
    
    name = result["name"]
    genres = result["genres"]
    spotify_url = result["external_urls"]["spotify"]
    spotify_id = result["id"]

    images = result["images"]
    image_url = ""
    if len(images) > 0: # If there is an image
        image_url = images[0]["url"]

    artists.append(artist(name, artist_id, [], genres, image_url, spotify_url, spotify_id))
    artist_id+=1

# Stores artists by alphabetical order
artists = sorted(artists, key=lambda artist: artist.name)
print(len(artists), "artists total")

# Add albums and their information
for i in artists:  
    # We can only get 50 albums at a time, so we need a while loop
    n = 0
    result = spotify.artist_albums(i.spotify_id, album_type="album", limit=50)
    while n <= result["total"]:
        result = result["items"]
        
        for j in result: # Each album
            name = j["name"]
            num_tracks = j["total_tracks"]
            release_day = j["release_date"]
            spotify_url = j["external_urls"]["spotify"]
            spotify_id = j["id"]

            album_artists = []
            for k in j["artists"]:
                album_artists.append(k["name"])

            images = j["images"]
            image_url = ""
            if len(images) > 0: # If there is an image
                image_url = images[0]["url"]

            curr_album = album(name, album_id, num_tracks, album_artists, [], image_url, release_day, spotify_url, spotify_id)
            if albums.count(curr_album) == 0: # We have not added this album yet
                albums.append(curr_album)
                album_id+=1

                # Finds each artist of this album"s artist object and add album to their albums list
                for l in curr_album.artists:
                    for m in artists:
                        if l == m.name:
                            m.albums.append(curr_album.name)
                            break
        n+=50
        result = spotify.artist_albums(i.spotify_id, album_type="album", limit=50, offset=n)
        
        print("Completed", artists.index(i) + 1, "/", len(artists), "artist[s]' albums list", end="\r")
print()

# Add singles and their information
for i in artists:    
    # We can only get 50 singles at a time, so we need a while loop
    n = 0
    result = spotify.artist_albums(i.spotify_id, album_type="single", limit=50)
    while n <= result["total"]:
        result = result["items"]
        
        for j in result: # Each album
            name = j["name"]
            num_tracks = j["total_tracks"]
            release_day = j["release_date"]
            spotify_url = j["external_urls"]["spotify"]
            spotify_id = j["id"]

            album_artists = []
            for k in j["artists"]:
                album_artists.append(k["name"])

            images = j["images"]
            image_url = ""
            if len(images) > 0: # If there is an image
                image_url = images[0]["url"]

            curr_album = album(name, album_id, num_tracks, album_artists, [], image_url, release_day, spotify_url, spotify_id)
            if albums.count(curr_album) == 0: # We have not added this single yet
                albums.append(curr_album)
                album_id+=1

                # Finds each artist of this album"s artist object and add single to their albums list
                for l in curr_album.artists:
                    for m in artists:
                        if l == m.name:
                            m.albums.append(curr_album.name)
                            break
        n+=50
        result = spotify.artist_albums(i.spotify_id, album_type="single", limit=50, offset=n)
        
        print("Added", artists.index(i) + 1, "/", len(artists), "artist[s]' singles to their albums list", end="\r")
print()

# Sort albums and singles by alphabetical order
albums = sorted(albums, key=lambda album: album.name)
print(len(albums), "albums total")

# Add artists to database
for i in artists:
    currArtist = Artist(i.name, i.id, i.genres, i.image_url, i.spotify_url, i.spotify_id, True)
    db.session.add(currArtist)
    Artists.append(currArtist)
    print("Working on artist", artists.index(i) + 1, "out of", len(artists), end="\r")
print()
db.session.commit()

# Add albums to database and link albums to their artists
for i in albums:
    currAlbum = Album(i.name, i.id, i.num_tracks, i.image_url, i.release_day, i.spotify_url, i.spotify_id)
    db.session.add(currAlbum)
    Albums.append(currAlbum)
    for j in i.artists:
        currArtist = Artist(j, artist_id)
        if Artists.count(currArtist) == 0: # First time seeing this artist, create new artist object
            db.session.add(currArtist)
            Artists.append(currArtist)
            artist_id+=1
        else:
            currArtist = Artists[Artists.index(currArtist)]
        currAlbum.album_artists.append(currArtist)
        print("Working on album", albums.index(i) + 1, "out of", len(albums), end="\r")
print()
db.session.commit()

# Add tracks to database and link tracks to their albums and artists
for i in Albums:
    # We can only get 50 tracks at a time, so we need a while loop
    n = 0
    result = spotify.album_tracks(i.album_spotify_id, limit=50)
    while n <= result["total"]:
        result = result["items"]
        
        for j in result: # Each track
            name = j["name"]
            duration = j["duration_ms"]/1000
            spotify_url = j["external_urls"]["spotify"]
            spotify_id = j["id"]
            preview_url = j["preview_url"]
            currTrack = Track(track_id=track_id, track_name=name, track_duration=duration, 
                              track_release_day=i.album_release_day, track_image_url=i.album_image_url, 
                              track_spotify_url=spotify_url, track_preview_url=preview_url, track_spotify_id=spotify_id,
                              track_album=i)
            db.session.add(currTrack)
            
            # Add artists
            for j in i.album_artists:
                currTrack.track_artists.append(j)

            # Add track to Track list
            Tracks.append(currTrack)
            track_id+=1
        
        n+=50
        result = spotify.album_tracks(i.album_spotify_id, limit=50, offset=n)
        
        print("Completed", Albums.index(i) + 1, "/", len(Albums), "albums[s]' tracks list", end="\r")
print()
db.session.commit()

print(len(Tracks), "tracks total")
print("Success")